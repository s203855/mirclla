function plotSignals(signals, fs, legendText)
    
    % signals is 2 dim array, with first dim being the signal number and second being the signal itself
    figure("Name", "Signals from variables");
    %sig = rescale(signals(:,i),-1,1);
    len = length(signals);
    % plot
    % figure("Name", "Signal: " + string(i))
    set(gca, 'XTickLabel', get(gca, 'XTick'));
    % -time
    subplot(2,1,1);
    t = (0:(1/fs):(len/fs - 1/fs)); %seconds
    plot(t,signals);
    title("Time Domain");
    nCol = size(signals,2);
    if nCol > 8
       nCol = 8;
    end
    legend(legendText,"NumColumns",nCol,Location="northoutside");
    % -frequency
    subplot(2, 1, 2)
    f = linspace(0,1,len/2 + 1)*fs/2; % hertz
    fSignal = (fft(signals));
    fSignal = abs(fSignal);
    fSignal = fSignal(1:len/2 + 1,:);
    semilogx(f,20*log10(fSignal),LineWidth=0.1);
    title("Frequency Domain");
    % xlim([20, fs/2]);
    % ylim([-5,5]);
    xlabel("f (Hz)");
    ylabel("dB");
end
