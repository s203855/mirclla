function speakers = ring2Speakers(rings)
    if isscalar(rings)
        switch rings
            case 1
                speakers = 1:2;
            case 2
                speakers = 3:8;
            case 3
                speakers = 9:20;
            case 4
                speakers = 21:44;
            case 5
                speakers = 45:56;
            case 6
                speakers = 57:62;
            case 7
                speakers = 63:64;
            otherwise
                error("Not a valid ring number, only from 1-7");
        end
    else
        for i = 1:length(rings)
            switch i
                case 1
                    speakers = [speakers, 1:2];
                case 2
                    speakers = [speakers, 3:8];
                case 3
                    speakers = [speakers, 9:20];
                case 4
                    speakers = [speakers, 21:44];
                case 5
                    speakers = [speakers, 45:56];
                case 6
                    speakers = [speakers, 57:62];
                case 7
                    speakers = [speakers, 63:64];
                otherwise
                    error("Not a valid ring number, only from 1-7");
            end
        end
    end
end