%% Clear vars
clear


%% Measure speakers
% Signal params
fStart = 40;
fStop = 24000;
fs = 48000;
dur = 3; %seconds
volume = -22; %dB of signal
mic = 1; % 1: Hanging mic, 5: recording mic

for i = 1:2
    disp(i)
   [res,exitation] = measureResponse(21,mic, dur, 0.3, fs, volume, [fStart fStop]);
   % ir = impzest(exitation, res);
   % audiowrite("C:\Users\s203855\Desktop\mirclla-base\measurements\" + "speaker21Mic1Run" + string(i) + ".wav", res, fs);
end
% plotSignals([ir], fs)

%figure,
%spectrogram(res(:,2),1024,512,1024,fs,'yaxis')

%[Hs,f] = gsmoothspec(f,H,n);

