function output = truncIR(startTime, endTime, fs, input)
    % Truncate input based on start and end time
    % startTime: Cutoff time before peak in seconds
    % endTime:   Cutoff time after peak in seconds
    % fs:        Sampling frequency of input
    % input:     Impulse response to be truncated

    startSample = startTime*fs;
    if startSample < 1
        startSample = 1;
    end
    
    endSample = endTime * fs;
    if endSample > length(input)
        endSample = length(input);
    end
    
    output = input(round(startSample):round(endSample)-1,:);

end