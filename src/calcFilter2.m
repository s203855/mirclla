function filt = calcFilter2(ir)
    nfft = 2^nextpow2(length(ir));
    H = fft(ir,nfft);
    % Get positive spectrum
    H = H(1:nfft/2 + 1);
    % Magnitude
    H = abs(H);
    % Normalized frequency
    f = linspace(0,1,length(H))';
    % Calculate filter
    filt = fir2(nfft,f,1./H);
    % Minimum phase
    [~,filt] = rceps(filt);
end