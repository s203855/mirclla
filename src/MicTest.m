% Compare speaker 21 from both mics

fs = 48000;

% mic1 = audioread("C:\Users\s203855\Desktop\mirclla-base\measurements\measurementSpeaker21Mic1.wav");
% mic5 = audioread("C:\Users\s203855\Desktop\mirclla-base\measurements\measurementSpeaker21Mic5.wav");
mic1 = audioread("measurements/measurementSpeaker21Mic1.wav");
mic5 = audioread("measurements/measurementSpeaker21Mic5.wav");
diff = mic1 - mic5;
mics = [mic1 mic5 diff];


excitation = audioread("measurements/excitationSignal.wav");
ir1 = impzest(excitation,mic1);
ir5 = impzest(excitation,mic5);
irDiff = ir1 - ir5;
irs = [ir1 ir5 irDiff];

[t1,t2,d]=alignsignals(ir1,ir5);
t2 = t2(1:length(t2)-abs(d));
plot([rescale(ir1,-1,1),rescale(ir5,-1,1)])

%%
% figure("Name", "Measurement")
% set(gca, 'XTickLabel', get(gca, 'XTick'));
% % -time
% subplot(2,1,1);
% hold on
% title("Time Domain");
% len = length(mics(:, 1));
% t = (0:(1/fs):(len/fs - 1/fs)); %seconds
% for i = 1:3
%     plot(t,mics(:, i))
% end
% legend("Mic1", "Mic5", "Diff")
% hold off
% % -frequency
% subplot(2, 1, 2);
% hold on
% f = fs/len * (0:len/2); % hertz
% for i = 1:3
%     fSignal = (fft(mics(:, i)));
%     fSignal = abs(fSignal/len);
%     fSignal = fSignal(1:len/2+1);
%     fSignal(2:end-1) = 2*fSignal(2:end-1);
%     semilogx(f,db(fSignal),LineWidth=0.1);
% end
% legend("Mic1", "Mic5", "Diff")
% title("Frequency Domain");
% ax = gca;
% ax.XAxis.Exponent = 0;
% xlabel("f (Hz)");
% %xlim([0 100])
% ylabel("|fft(X)|");
% hold off

figure("Name", "Impulse Response")
set(gca, 'XTickLabel', get(gca, 'XTick'));
% -time
subplot(2,1,1);
hold on
title("Time Domain");
len = length(irs(:, 1));
t = (0:(1/fs):(len/fs - 1/fs)); %seconds
for i = 1:3
    plot(t,irs(:, i))
end
legend("Mic1", "Mic5", "Diff")
hold off
% -frequency
subplot(2, 1, 2);
hold on
f = fs/len * (0:len/2); % hertz
for i = 1:3
    fSignal = (fft(irs(:, i)));
    fSignal = abs(fSignal/len);
    fSignal = fSignal(1:len/2+1);
    fSignal(2:end-1) = 2*fSignal(2:end-1);
    semilogx(f,db(fSignal),LineWidth=0.1);
end
legend("Mic1", "Mic5", "Diff")
title("Frequency Domain");
% ax = gca;
% ax.XAxis.Exponent = 0;
xlabel("f (Hz)");
%xlim([0 100])
ylabel("|fft(X)|");
hold off