function plotRings(ringN, fs, path, mic)
% Plot all the impulse response of all the speakers from a given ring
% Plot in both time an freq domain
% Reads looks for filenames of the structure "measurmentSpeakerXXMicX.wav" and for the file "excitationSignal"

switch ringN
    case 1
        signals = 1:2;
    case 2
        signals = 3:8;
    case 3
        signals = 9:20;
    case 4
        signals = 21:44;
    case 5
        signals = 45:56;
    case 6
        signals = 57:62;
    case 7
        signals = 63:64;
    otherwise
        error("Not a valid ring number, only from 1-7");
end

% disp("Reading excitation Signal")
% excitationSignal = audioread(path + "excitationSignal.wav");
% length(excitationSignal)

disp("Reading Signals")
for i = 1:length(signals)
    [sig, cfs] = audioread(path + "measurementSpeaker" + string(signals(i)) + "Mic" + string(mic) + ".wav");
    % length(sig)
    if cfs ~= fs
        error("Error. file sampling frequency (%d) does not match given sampling frequency (%d)", cfs, fs);
        break
    end
    if exist("sigs", "var") == 0
        sigs = zeros(length(sig), length(signals));
    end
    sigs(:,i) = sig;
end
disp("Plotting ring")
plotSignals(sigs, fs, string(signals));
 

