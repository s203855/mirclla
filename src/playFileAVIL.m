%% AVIL testing playback

function playFileAVIL(file)
% Audio player setup
fs = 48000;
deviceName = "RedNet PCIe";
bufferSize = 2048;
setpref("dsp", "portaudioHostApi", 3); % Tells matlab to use ASIO drivers
aDW = audioDeviceWriter("SampleRate", fs, "BufferSize", bufferSize, "Driver", "ASIO", "Device", deviceName, "ChannelMappingSource", "Property");
% aDW = audioDeviceWriter("SampleRate", fs, "BufferSize", bufferSize, "Driver", "ASIO", "Device", deviceName);

% File handling
frameLength = 2^12;
aFR = dsp.AudioFileReader("Filename", file, "SamplesPerFrame", frameLength);

% Channel check and mapping
channelNum = 21; %vector with same number of channels as in audiofile. Only used if audiofile is is not 64 channels.

if aFR.info.NumChannels == 64
    aDW.ChannelMapping = 1:64;
else
    aDW.ChannelMapping = channelNum;
end

% Playing
disp("Playing")
while ~aFR.isDone
    aData = aFR(); % Reading one frame
    aData = 0.06*aData; % Scaling
    aDW(aData);
end
disp("Done")
pause(1)
release(aFR);
release(aDW);
end
