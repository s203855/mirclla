

p = "/home/andreas/Documents/Programming/mirclla/measurements/";
% p = "C:\Users\s203855\Desktop\mirclla-HANDIN\measurements\";
fs = 48000;
excitationSignal = audioread(p + "excitationSignal.wav");
lw = 2;
xrange = [40, fs/2];

%% Horizontal slice (limit to max 8)
% plotRings(4,48000,path,5);
yrange = [-10, 10];
speakers = 21:3:44;
% speakers = [21];
for i = 1:length(speakers)
    [sig, cfs] = audioread(p + "measurementSpeaker" + string(speakers(i)) + "Mic5" + ".wav");
    if cfs ~= fs
        error("Error. file sampling frequency (%d) does not mat filtered given sampling frequency (%d)", cfs, fs);
    end
    if exist("sigs", "var") == 0
        sigs = zeros(length(sig), length(speakers));
    end
    sigs(:,i) = sig;
end

IRs = impzest(excitationSignal,sigs);
IRs = truncIR(0.110,0.112,fs,IRs);
tmp = zeros(2048,size(IRs,2));
tmp(1:size(IRs,1),1:size(IRs,2)) = IRs;
IRs = tmp;
clear tmp

for i = 1:size(IRs,2)
    IRs(:,i) = scaleIR(IRs(:,i));
end
clear sigs

IRsfilt = zeros(size(IRs,1),size(IRs,2));
filtered = zeros(size(IRs,1)*2, size(IRs,2));
for i = 1:size(IRs,2)
    filt = calcFilter2(IRs(:,i));
    IRsfilt(:,i) = filt(1:size(IRs,1));
    filtered(:,i) = conv(IRs(:,i),filt);
end

% Before filter
% Freq
figure("Name","Ring 4 IR");
set(gca, 'XTickLabel', get(gca, 'XTick'));
nfft = 2^nextpow2(length(filtered));
f = linspace(0,1,nfft/2 + 1)*fs/2; % hertz
fSignal = (fft(IRs,nfft));
fSignal = abs(fSignal);
fSignal = fSignal(1:nfft/2 + 1,:);
semilogx(f,20*log10(fSignal),LineWidth=lw);
xlim(xrange);
ylim(yrange);
title("Frequency Domain");
xlabel("f (Hz)");
ylabel("dB");
legend(string(speakers));
grid on
export_figure(gcf, "Ring4IrFreq", 4*1000, 3*1000, 500);

% Time
figure("Name","Ring 4 IR");
t = (0:(1/fs):(length(IRs)/fs - 1/fs)); %seconds
plot(t*1000,IRs,LineWidth=lw);
xlim([0 1]);
ylim([-1, 2]);
title("Time Domain");
xlabel("t (ms)");
ylabel("Amplitude");
legend(string(speakers));
grid on
export_figure(gcf, "Ring4IrTime", 4*1000, 3*1000, 500);

% After filter
figure("Name","Ring 4 filtered")
set(gca, 'XTickLabel', get(gca, 'XTick'));
nfft = 2^nextpow2(length(filtered));
f = linspace(0,1,nfft/2 + 1)*fs/2; % hertz
fSignal = (fft(filtered,nfft));
fSignal = abs(fSignal);
fSignal = fSignal(1:nfft/2 + 1,:);
semilogx(f,20*log10(fSignal),LineWidth=lw);
xlim(xrange);
ylim(yrange);
title("Frequency Domain");
xlabel("f (Hz)");
ylabel("dB");
legend(string(speakers));
grid on
export_figure(gcf, "Ring4FilteredFreq", 4*1000, 3*1000, 500);

% Time
figure("Name","Ring 4 Filtered");
t = (0:(1/fs):(length(filtered)/fs - 1/fs)); %seconds
plot(t*1000,filtered,LineWidth=lw);
xlim([0 1]);
ylim([-1, 2]);
title("Time Domain");
xlabel("t (ms)");
ylabel("Amplitude");
legend(string(speakers));
grid on
export_figure(gcf, "Ring4FilteredTime", 4*1000, 3*1000, 500);

%% Vertical slice
speakers = [1,3,9,21,45,57,63];
yrange = [-30, 5];
for i = 1:length(speakers)
    [sig, cfs] = audioread(p + "measurementSpeaker" + string(speakers(i)) + "Mic5" + ".wav");
    if cfs ~= fs
        error("Error. file sampling frequency (%d) does not mat filtered given sampling frequency (%d)", cfs, fs);
         
    end
    if exist("sigs", "var") == 0
        sigs = zeros(length(sig), length(speakers));
    end
    sigs(:,i) = sig;
end

IRs = impzest(excitationSignal,sigs);
IRs = truncIR(0.110,0.112,fs,IRs);
tmp = zeros(2048,size(IRs,2));
tmp(1:size(IRs,1),1:size(IRs,2)) = IRs;
IRs = tmp;
clear tmp

for i = 1:size(IRs,2)
    IRs(:,i) = scaleIR(IRs(:,i));
end
clear sigs

IRsfilt = zeros(size(IRs,1),size(IRs,2));
filtered = zeros(size(IRs,1)*2, size(IRs,2));
for i = 1:size(IRs,2)
    filt = calcFilter2(IRs(:,i));
    IRsfilt(:,i) = filt(1:size(IRs,1));
    filtered(:,i) = conv(IRs(:,i),filt); 
end

% Before filter
figure("Name", "Vertical slice IR");
set(gca, 'XTickLabel', get(gca, 'XTick'));
nfft = 2^nextpow2(length(IRs));
f = linspace(0,1,nfft/2 + 1)*fs/2; % hertz
fSignal = (fft(IRs,nfft));
fSignal = abs(fSignal);
fSignal = fSignal(1:nfft/2 + 1,:);
semilogx(f,20*log10(fSignal),LineWidth=lw);
xlim(xrange);
ylim(yrange);
title("Frequency Domain");
xlabel("f (Hz)");
ylabel("dB");
legend("Ring 1","Ring 2","Ring 3","Ring 4","Ring 5","Ring 6","Ring 7");
grid on
export_figure(gcf, "VertSliceIrFreq", 4*1000, 3*1000, 500);

% Time
figure("Name", "Vertical slice IR");
t = (0:(1/fs):(length(IRs)/fs - 1/fs)); %seconds
plot(t*1000,IRs,LineWidth=lw);
xlim([0 2]);
ylim([-1, 2]);
title("Time Domain");
xlabel("t (ms)");
ylabel("Amplitude");
legend("Ring 1","Ring 2","Ring 3","Ring 4","Ring 5","Ring 6","Ring 7");
grid on
export_figure(gcf, "VertSliceIrTime", 4*1000, 3*1000, 500);


% After filter
figure("Name","Vertical slice filtered")
set(gca, 'XTickLabel', get(gca, 'XTick'));
nfft = 2^nextpow2(length(filtered));
f = linspace(0,1,nfft/2 + 1)*fs/2; % hertz
fSignal = (fft(filtered,nfft));
fSignal = abs(fSignal);
fSignal = fSignal(1:nfft/2 + 1,:);
semilogx(f,20*log10(fSignal),LineWidth=lw);
xlim(xrange);
ylim(yrange);
title("Frequency Domain");
xlabel("f (Hz)");
ylabel("dB");
legend("Ring 1","Ring 2","Ring 3","Ring 4","Ring 5","Ring 6","Ring 7");
grid on
export_figure(gcf, "VertSliceFilteredFreq", 4*1000, 3*1000, 500);

% Time
figure("Name", "Vertical slice filtered");
t = (0:(1/fs):(length(filtered)/fs - 1/fs)); %seconds
plot(t*1000,filtered,LineWidth=lw);
xlim([0 2]);
ylim([-1, 2]);
title("Time Domain");
xlabel("t (ms)");
ylabel("Amplitude");
legend("Ring 1","Ring 2","Ring 3","Ring 4","Ring 5","Ring 6","Ring 7");
grid on
export_figure(gcf, "VertSliceFilteredTime", 4*1000, 3*1000, 500);


%% Mic 1 comparison

% Vertical
speakers = [1,3,9,21,45,57,63];
yrange = [-15, 10];
for i = 1:length(speakers)
    [sig, cfs] = audioread(p + "measurementSpeaker" + string(speakers(i)) + "Mic1" + ".wav");
    if cfs ~= fs
        error("Error. file sampling frequency (%d) does not mat filtered given sampling frequency (%d)", cfs, fs);
    end
    if exist("sigs", "var") == 0
        sigs = zeros(length(sig), length(speakers));
    end
    sigs(:,i) = sig;
end

IRs = impzest(excitationSignal,sigs);
IRs = truncIR(0.107,0.114,fs,IRs);
tmp = zeros(2048,size(IRs,2));
tmp(1:size(IRs,1),1:size(IRs,2)) = IRs;
IRs = tmp;
clear tmp
for i = 1:size(IRs,2)
    IRs(:,i) = scaleIR(IRs(:,i));
end
clear sigs

% Freq
figure("Name", "Microphone1 IR");
set(gca, 'XTickLabel', get(gca, 'XTick'));
nfft = 2^nextpow2(length(IRs));
f = linspace(0,1,nfft/2 + 1)*fs/2; % hertz
fSignal = (fft(IRs,nfft));
fSignal = abs(fSignal);
fSignal = fSignal(1:nfft/2 + 1,:);
semilogx(f,20*log10(fSignal),LineWidth=lw);
xlim(xrange);
ylim(yrange);
title("Frequency Domain");
xlabel("f (Hz)");
ylabel("dB");
legend("Ring 1","Ring 2","Ring 3","Ring 4","Ring 5","Ring 6","Ring 7");
grid on
export_figure(gcf, "Mic1VertIrFreq", 4*1000, 3*1000, 500);

% Time
figure("Name", "Microphone1 IR");
t = (0:(1/fs):(length(IRs)/fs - 1/fs)); %seconds
plot(t*1000,IRs,LineWidth=lw);
xlim([0 7]);
ylim([-1, 1]);
title("Time Domain");
xlabel("t (ms)");
ylabel("Amplitude");
legend("Ring 1","Ring 2","Ring 3","Ring 4","Ring 5","Ring 6","Ring 7");
grid on
export_figure(gcf, "Mic1VertIrTime", 4*1000, 3*1000, 500);

% Ring 4
speakers = 21:3:44;
for i = 1:length(speakers)
    [sig, cfs] = audioread(p + "measurementSpeaker" + string(speakers(i)) + "Mic1" + ".wav");
    if cfs ~= fs
        error("Error. file sampling frequency (%d) does not mat filtered given sampling frequency (%d)", cfs, fs);
    end
    if exist("sigs", "var") == 0
        sigs = zeros(length(sig), length(speakers));
    end
    sigs(:,i) = sig;
end

IRs = impzest(excitationSignal,sigs);
IRs = truncIR(0.107,0.114,fs,IRs);
tmp = zeros(2048,size(IRs,2));
tmp(1:size(IRs,1),1:size(IRs,2)) = IRs;
IRs = tmp;
clear tmp
for i = 1:size(IRs,2)
    IRs(:,i) = scaleIR(IRs(:,i));
end
clear sigs

% Freq
figure("Name","Microphone1 IR")
set(gca, 'XTickLabel', get(gca, 'XTick'));
nfft = 2^nextpow2(length(IRs));
f = linspace(0,1,nfft/2 + 1)*fs/2; % hertz
fSignal = (fft(IRs,nfft));
fSignal = abs(fSignal);
fSignal = fSignal(1:nfft/2 + 1,:);
semilogx(f,20*log10(fSignal),LineWidth=lw);
xlim(xrange);
ylim(yrange);
title("Frequency Domain");
xlabel("f (Hz)");
ylabel("dB");
legend(string(speakers));
grid on
export_figure(gcf, "Mic1Ring4IrFreq", 4*1000, 3*1000, 500);

% Time
figure("Name", "Microphone1 IR");
t = (0:(1/fs):(length(IRs)/fs - 1/fs)); %seconds
plot(t*1000,IRs,LineWidth=lw);
xlim([3, 5]);
ylim([-1, 1]);
title("Time Domain");
xlabel("t (ms)");
ylabel("Amplitude");
legend(string(speakers));
grid on
export_figure(gcf, "Mic1Ring4IrTime", 4*1000, 3*1000, 500);

%% Filter comparison

yrange = [-10, 10];
speakers = 21:3:44;
% speakers = [21];
for i = 1:length(speakers)
    [sig, cfs] = audioread(p + "measurementSpeaker" + string(speakers(i)) + "Mic5" + ".wav");
    if cfs ~= fs
        error("Error. file sampling frequency (%d) does not mat filtered given sampling frequency (%d)", cfs, fs);
         
    end
    if exist("sigs", "var") == 0
        sigs = zeros(length(sig), length(speakers));
    end
    sigs(:,i) = sig;
end

% LSQ filter
LSQtime = zeros(5,1);
for num = 1:length(LSQtime)
    tic
    IRs = impzest(excitationSignal,sigs);
    IRs = truncIR(0.110,0.112,fs,IRs);
    tmp = zeros(2048,size(IRs,2));
    tmp(1:size(IRs,1),1:size(IRs,2)) = IRs;
    IRs = tmp;
    clear tmp
    
    for i = 1:size(IRs,2)
        IRs(:,i) = scaleIR(IRs(:,i));
    end
    
    IRsfilt = zeros(size(IRs,1),size(IRs,2));
    filtered = zeros(size(IRs,1)*2 - 1, size(IRs,2));
    for i = 1:size(IRs,2)
        filt = calcFilter(IRs(:,i));
        [~, filt] = rceps(filt);
        IRsfilt(:,i) = filt;
        filtered(:,i) = conv(IRs(:,i),filt); 
    end
    LSQtime(num) = toc;
end

figure("Name","LSQ filter")
set(gca, 'XTickLabel', get(gca, 'XTick'));
nfft = 2^nextpow2(length(filtered));
f = linspace(0,1,nfft/2 + 1)*fs/2; % hertz
fSignal = (fft(filtered,nfft));
fSignal = abs(fSignal);
fSignal = fSignal(1:nfft/2 + 1,:);
semilogx(f,20*log10(fSignal),LineWidth=lw);
xlim(xrange);
ylim(yrange);
title("Frequency Domain");
xlabel("f (Hz)");
ylabel("dB");
legend(string(speakers));
grid on
export_figure(gcf, "LSQFilteredFreq", 4*1000, 3*1000, 500);

% Time
figure("Name", "LSQ filter");
t = (0:(1/fs):(length(filtered)/fs - 1/fs)); %seconds
plot(t*1000,filtered,LineWidth=lw);
xlim([0, 2]);
ylim([-1, 2]);
title("Time Domain");
xlabel("t (ms)");
ylabel("Amplitude");
legend(string(speakers));
grid on
export_figure(gcf, "LSQFilteredTime", 4*1000, 3*1000, 500);


% Inverted response filter
invTime = zeros(5,1);
for num = 1:length(invTime)
    tic
    IRs = impzest(excitationSignal,sigs);
    IRs = truncIR(0.110,0.112,fs,IRs);
    tmp = zeros(2048,size(IRs,2));
    tmp(1:size(IRs,1),1:size(IRs,2)) = IRs;
    IRs = tmp;
    clear tmp
    
    nfft = 2^nextpow2(length(IRs));
    H = fft(IRs,nfft);
    H = H(1:nfft/2 + 1,:);
    H = abs(H);
    f = linspace(0,1,length(H))';
    IRsfilt = zeros(size(IRs,1),size(IRs,2));
    filtered = zeros(size(IRs,1)*2 - 1, size(IRs,2));
    for i = 1:size(IRs,2)
        filt = fir2(nfft,f,1./H(:,i));
        [~,filt] = rceps(filt);
        IRsfilt(:,i) = filt(1:nfft);
        filtered(:,i) = conv(IRs(:,i),filt(1:nfft));
    end
    invTime(num) = toc;
end

figure("Name","Inverted response filter")
set(gca, 'XTickLabel', get(gca, 'XTick'));
nfft = 2^nextpow2(length( filtered));
f = linspace(0,1,nfft/2 + 1)*fs/2; % hertz
fSignal = (fft( filtered,nfft));
fSignal = abs(fSignal);
fSignal = fSignal(1:nfft/2 + 1,:);
semilogx(f,20*log10(fSignal),LineWidth=lw);
xlim(xrange);
ylim(yrange);
title("Frequency Domain");
xlabel("f (Hz)");
ylabel("dB");
legend(string(speakers));
grid on
export_figure(gcf, "InvFilteredFreq", 4*1000, 3*1000, 500);

% Time
figure("Name","Inverted response filter")
t = (0:(1/fs):(length(filtered)/fs - 1/fs)); %seconds
plot(t*1000,filtered,LineWidth=lw);
xlim([0, 2]);
ylim([-1, 2]);
title("Time Domain");
xlabel("t (ms)");
ylabel("Amplitude");
legend(string(speakers));
grid on
export_figure(gcf, "InvFilteredTime", 4*1000, 3*1000, 500);

disp("LSQ processing time was: " + string(mean(LSQtime)) + "s");
disp("Inv processing time was: " + string(mean(invTime)) + "s");


%% Processing time

speakers = 21:3:44;
% speakers = [21];
for i = 1:length(speakers)
    [sig, cfs] = audioread(p + "measurementSpeaker" + string(speakers(i)) + "Mic5" + ".wav");
    if cfs ~= fs
        error("Error. file sampling frequency (%d) does not mat filtered given sampling frequency (%d)", cfs, fs);

    end
    if exist("sigs", "var") == 0
        sigs = zeros(length(sig), length(speakers));
    end
    sigs(:,i) = sig;
end

% With truncation
t_proc = zeros(5,1);
for num = 1:length(t_proc)
    tic
    IRs = impzest(excitationSignal,sigs);
    IRs = truncIR(0.110,0.112,fs,IRs);
    tmp = zeros(2048,size(IRs,2));
    tmp(1:size(IRs,1),1:size(IRs,2)) = IRs;
    IRs = tmp;
    clear tmp
    
    for i = 1:size(IRs,2)
        IRs(:,i) = scaleIR(IRs(:,i));
    end
    
    IRsfilt = zeros(size(IRs,1),size(IRs,2));
    filtered = zeros(size(IRs,1)*2, size(IRs,2));
    for i = 1:size(IRs,2)
        filt = calcFilter2(IRs(:,i));
        IRsfilt(:,i) = filt(1:size(IRs,1));
        filtered(:,i) = conv(IRs(:,i),filt);
    end
    t_proc(num) = toc;
end

% Without truncation
t_uproc = zeros(5,1);
for num = 1:length(t_uproc)
    tic
    IRs = impzest(excitationSignal,sigs);
    % IRs = truncIR(0.110,0.112,fs,IRs);
    tmp = zeros(2048,size(IRs,2));
    tmp(1:size(IRs,1),1:size(IRs,2)) = IRs;
    IRs = tmp;
    clear tmp
    
    for i = 1:size(IRs,2)
        IRs(:,i) = scaleIR(IRs(:,i));
    end
    
    IRsfilt = zeros(size(IRs,1),size(IRs,2));
    filtered = zeros(size(IRs,1)*2 - 1, size(IRs,2));
    for i = 1:size(IRs,2)
        filt = calcFilter2(IRs(:,i));
        IRsfilt(:,i) = filt(1:size(IRs,1));
        filtered(:,i) = conv(IRs(:,i),filt(1:size(IRs,1)));
    end
    t_uproc(num) = toc;
end

disp("Processed time was: " + string(mean(t_proc)*1000) + "ms")
disp("Unprocessed time was: " + string(mean(t_uproc)*1000) + "ms")

%% Min-phase difference

yrange = [-10, 10];
speakers = 21:3:44;
% speakers = [21];
for i = 1:length(speakers)
    [sig, cfs] = audioread(p + "measurementSpeaker" + string(speakers(i)) + "Mic5" + ".wav");
    if cfs ~= fs
        error("Error. file sampling frequency (%d) does not mat filtered given sampling frequency (%d)", cfs, fs);
         
    end
    if exist("sigs", "var") == 0
        sigs = zeros(length(sig), length(speakers));
    end
    sigs(:,i) = sig;
end

IRs = impzest(excitationSignal,sigs);
IRs = truncIR(0.110,0.112,fs,IRs);
tmp = zeros(2048,size(IRs,2));
tmp(1:size(IRs,1),1:size(IRs,2)) = IRs;
IRs = tmp;
clear tmp

% Without min-phase
nfft = 2^nextpow2(length(IRs));
H = fft(IRs,nfft);
H = H(1:nfft/2 + 1,:);
H = abs(H);
f = linspace(0,1,length(H))';
IRsfilt = zeros(size(IRs,1),size(IRs,2));
filtered = zeros(size(IRs,1)*2 - 1, size(IRs,2));
for i = 1:size(IRs,2)
    filt = fir2(nfft,f,1./H(:,i));
    % [~,filt] = rceps(filt);
    IRsfilt(:,i) = filt(1:nfft);
    filtered(:,i) = conv(IRs(:,i),filt(1:nfft));
end

figure("Name","Inverted response filter")
set(gca, 'XTickLabel', get(gca, 'XTick'));
nfft = 2^nextpow2(length( filtered));
f = linspace(0,1,nfft/2 + 1)*fs/2; % hertz
fSignal = (fft( filtered,nfft));
fSignal = abs(fSignal);
fSignal = fSignal(1:nfft/2 + 1,:);
semilogx(f,20*log10(fSignal),LineWidth=lw);
xlim(xrange);
ylim(yrange);
title("Frequency Domain");
xlabel("f (Hz)");
ylabel("dB");
legend(string(speakers));
grid on
export_figure(gcf, "NoMinPhaseFreq", 4*1000, 3*1000, 500);


% Time
figure("Name","Inverted response filter")
t = (0:(1/fs):(length(filtered)/fs - 1/fs)); %seconds
plot(t*1000,filtered,LineWidth=lw);
xlim([20, 25]);
ylim([-1, 1]);
title("Time Domain");
xlabel("t (ms)");
ylabel("Amplitude");
legend(string(speakers));
grid on
export_figure(gcf, "NoMinPhaseTime", 4*1000, 3*1000, 500);

% Min-phase
nfft = 2^nextpow2(length(IRs));
H = fft(IRs,nfft);
H = H(1:nfft/2 + 1,:);
H = abs(H);
f = linspace(0,1,length(H))';
IRsfilt = zeros(size(IRs,1),size(IRs,2));
filtered = zeros(size(IRs,1)*2 - 1, size(IRs,2));
for i = 1:size(IRs,2)
    filt = fir2(nfft,f,1./H(:,i));
    [~,filt] = rceps(filt);
    IRsfilt(:,i) = filt(1:nfft);
    filtered(:,i) = conv(IRs(:,i),filt(1:nfft));
end


figure("Name","Min-phase")
set(gca, 'XTickLabel', get(gca, 'XTick'));
nfft = 2^nextpow2(length( filtered));
f = linspace(0,1,nfft/2 + 1)*fs/2; % hertz
fSignal = (fft( filtered,nfft));
fSignal = abs(fSignal);
fSignal = fSignal(1:nfft/2 + 1,:);
semilogx(f,20*log10(fSignal),LineWidth=lw);
xlim(xrange);
ylim(yrange);
title("Frequency Domain");
xlabel("f (Hz)");
ylabel("dB");
legend(string(speakers));
grid on
export_figure(gcf, "MinPhaseFreq", 4*1000, 3*1000, 500);

% Time
figure("Name","Min-phase")
t = (0:(1/fs):(length(filtered)/fs - 1/fs)); %seconds
plot(t*1000,filtered,LineWidth=lw);
xlim([0, 5]);
ylim([-1, 1]);
title("Time Domain");
xlabel("t (ms)");
ylabel("Amplitude");
legend(string(speakers));
grid on
export_figure(gcf, "MinPhaseTime", 4*1000, 3*1000, 500);

%% Truncation point plot

% speakers = 21:44; 40
% speakers = [1,3,9,21,45,57,63];
speakers = [40, 45];
for i = 1:length(speakers)
    [sig, cfs] = audioread(p + "measurementSpeaker" + string(speakers(i)) + "Mic5" + ".wav");
    if cfs ~= fs
        error("Error. file sampling frequency (%d) does not mat filtered given sampling frequency (%d)", cfs, fs);
         
    end
    if exist("sigs", "var") == 0
        sigs = zeros(length(sig), length(speakers));
    end
    sigs(:,i) = sig;
end

clear IRs
IRs = impzest(excitationSignal,sigs);
clear sigs

% Time
figure("Name","Truncation")
hold on
t = (0:(1/fs):(length(IRs)/fs - 1/fs)); %seconds
plot(t,IRs,LineWidth=lw);
xline(0.11, LineWidth=lw, Color="Green")
xline(0.112, LineWidth=lw,Color="Red")
xlim([0.11-0.001, 0.112+0.001]);
% ylim([-1, 1]);
title("Time Domain");
xlabel("t (s)");
ylabel("Amplitude");
legend([string(speakers), "First cut off", "After cut off"]);
grid on
export_figure(gcf, "CutoffPoints", 4*1000, 3*1000, 500);
