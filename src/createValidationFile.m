function createValidationFile(speakers, runsPerSpeaker, pDev)
    % INPUTS:
    % speakers: Speakers to create validation for
    % runsPerSpeaker: How many runs per speaker
    % pDev: Percent deviation from mean
    %
    % OUTPUT
    % File containing
    %   high values
    %   low values
    
    % Signal parameters
    fStart = 40;
    fStop = 24000;
    fs = 48000;
    dur = 3; %seconds
    endSilience = 0.3;
    volume = -22; %dB of signal
    mic = 1; % DPA mic

    for speaker = 1:length(speakers)
        
        % Measure response
        for i = 1:runsPerSpeaker
            [sig, excitation] = measureResponse(speakers(speaker),mic,dur,endSilience,fs,volume,[fStart, fStop]);
            if exist("sigs", "var") == 0
                sigs = zeros(length(sig), runsPerSpeaker);
            end
            sigs(:,i) = sig;
        end
        
        % Calculating impulse response
        IRs = impzest(excitation, sigs);
        
        % Processing
        IRs = truncIR(0.107,0.114,fs,IRs);
        tmp = zeros(2048,size(IRs,2));
        tmp(1:size(IRs,1),1:size(IRs,2)) = IRs;
        IRs = tmp;
        clear tmp
        for i = 1:size(IRs,2)
            IRs(:,i) = scaleIR(IRs(:,i));
        end
        
        % Transform to freq
        nfft = 2^nextpow2(length(IRs));
        IRsFreq = fft(IRs,nfft);
        IRsFreq = abs(IRsFreq);
        IRsFreq = IRsFreq(1:nfft/2 + 1,:);
        
        % Average over speakers
        avgIRsFreq = mean(IRsFreq,2);
        
        % Calculate ceiling and floor
        High = avgIRsFreq + (pDev/100) * avgIRsFreq;
        Low = avgIRsFreq - (pDev/100) * avgIRsFreq;
        
        output(:,speaker,1) = High;
        output(:,speaker,2) = Low;
    end

    save("output/validation.mat","output");

end