%% AVIL testing recording

function [recordedSignals, excitationSignal] = measureResponse(playbackChannel, recordingChannel, dur, endSilence, fs, vol, fRange)
disp("Starting") 

% Create excitationSignal
disp("Creating excitation signal")
excitationSignal = sweeptone(dur, endSilence, fs, "ExcitationLevel", vol, "SweepFrequencyRange", fRange);

% if ~isfile("C:\Users\s203855\Desktop\mirclla-base\excitationSignal.wav")
%     audiowrite("C:\Users\s203855\Desktop\mirclla-base\measurements\excitationSignal.wav", excitationSignal, fs);
% end

% Recording setup
disp("Setting up for recording")
frameLength = 2048; % For RedNetPCIe Card
dataLength = size(excitationSignal,1);
nFrames = ceil(dataLength/frameLength);
recordedSignals = zeros(dataLength, length(recordingChannel));

for speaker = 1:length(playbackChannel)
    aPR = audioPlayerRecorder("Device", "RedNet PCIe", "SampleRate", fs, "BitDepth","24-bit integer","PlayerChannelMapping",playbackChannel(speaker),"RecorderChannelMapping",recordingChannel);
    
    
    % Play and record
    recordedSignal = zeros(dataLength, 1);
    nUnderruns = zeros(nFrames, 1);
    nOverruns = zeros(nFrames, 1);
    
    disp("Recording")
    for i = 1:nFrames-1
        start = frameLength*(i-1)+1;
        stop = frameLength*i;
        [recordedSignal(start:stop), nUnderruns(i), nOverruns(i)] = aPR(excitationSignal(start:stop));
    end
    release(aPR)
    disp("Done recording")
    
    recordedSignals(:, speaker) = recordedSignal;

    % disp("Writing")
    % recordedSignals(i) = recordedSignal;
    % if length(recordingChannel) > 1
    %     tmp = string(recordingChannel);
    %     audiowrite("C:\Users\s203855\Desktop\mirclla-base\measurements\" + "measurementSpeaker" + string(playbackChannel(speaker)) + "Mic" + tmp(1) + "and" + tmp(2) + ".wav", recordedSignal, fs);
    % else
    %     audiowrite("C:\Users\s203855\Desktop\mirclla-base\measurements\" + "measurementSpeaker" + string(playbackChannel(speaker)) + "Mic" + recordingChannel + ".wav", recordedSignal, fs);
    % end
end
disp("Done")
end

