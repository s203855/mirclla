%% Method 1
disp("Generating signal")
dur = 3; %seconds
endSilence = 0.3; %seconds
fs = 48000;
volume = -22; %dB
fStart = 40;
fStop = 24000; 
sig = sweeptone(dur, endSilence, fs, "ExcitationLevel", volume, "SweepFrequencyRange", [fStart, fStop]);

nchannels = 5;
sigData = zeros(length(sig)*nchannels,1);

for i = 1:size(sigData,1)
    j = mod(i, length(sig)) + 1;
    sigData(i) = sig(j);
end

t = 0:1/fs:(nchannels*(dur+endSilence)) - (1/fs);
figure
pspectrum(sigData,fs,"spectrogram")

disp("Playing sound")
soundsc(sigData,fs,24,[-1,1])