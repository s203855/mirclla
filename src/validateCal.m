function [status, speaker] = validateCal()

    status = 1;
    speaker = 0;
    validation = load("output/validation"); % signal, speaker, (high,low)

    speakers = 1:64;
    fStart = 40;
    fStop = 24000;
    fs = 48000;
    dur = 3; %seconds
    endSilience = 0.3;
    volume = -22; %dB of signal
    mic = 1; % DPA mic
    [sigs, excitation] = measureResponse(speakers,mic,dur,endSilience,fs,volume,[fStart, fStop]);

    % Calculating impulse response
    IRs = impzest(excitation, sigs);
    
    % Processing
    IRs = truncIR(0.107,0.114,fs,IRs);
    tmp = zeros(2048,size(IRs,2));
    tmp(1:size(IRs,1),1:size(IRs,2)) = IRs;
    IRs = tmp;
    clear tmp
    for i = 1:size(IRs,2)
        IRs(:,i) = scaleIR(IRs(:,i));
    end

    for i = 1:length(speakers)
        if IRs(i) > validation(:, i, 1) || IRs(i) < validation(:, i, 2)
            status = 0;
            speaker = i;
        end
    end
end