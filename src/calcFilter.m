function filt = calcFilter(ir)
    % LSQ without regularization
    % From invert from avil-calibration git (HeaLSQInverter) 


    C = convmtx(ir,length(ir));
    
    % least squares solution
    Cinv = (C'*C)\C';
    
    % convolve for all solutions
    tmpIR = C*Cinv;

    % select solution with highest impulse response
    [~,n] = max(diag(tmpIR));
    filt = Cinv(:,n);
end

% 
% function coeff = equalize(tx,rx,depth,ntaps)
% %Determines equalizer coefficients using the Wiener-Hopf equations
% %TX = Transmitted (Desired) waveform, row vector, length must be > depth+2*ntaps
% %RX = Received (Distorted) waveform, row vector, length must be >=depth 
% %DEPTH = Depth of solution matrix (recommend 10x ntaps but based on duration of stationarity)
% %NTAPS = Number of taps for equalizer filter
% 
% %force row vectors
% tx= tx(:)';
% rx= rx(:)';
% 
% delay=floor(ntaps/2);
% A=convmtx(rx(1:depth).',ntaps);
% R=A'*A;
% X=[zeros(1,delay) tx(1:depth) zeros(1,ceil(ntaps/2)-1)].';
% ro=A'*X;
% coeff=(inv(R)*ro);