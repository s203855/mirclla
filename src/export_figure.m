function export_figure(fig, name, width_px, height_px, dpi)
    % Convert pixel dimensions to inches
    width_in = width_px / dpi;
    height_in = height_px / dpi;
    
    % Set figure properties
    set(gcf, 'renderer', 'painters');
    set(fig, 'PaperPositionMode', 'manual');
    set(fig, 'PaperUnits', "inches");
    set(fig, 'PaperPosition', [0, 0, width_in, height_in]); % First two are ignored when not printing a page document
    
    % Set Margin
    iset = get(gca,'TightInset');
    iset(3) = iset(1)/2;
    set(gca,'LooseInset',iset);

    % Save the figure with export_fig
    print(fig, "output/" + name, "-dpng", "-r" + num2str(dpi));

    % Close the figure if it's no longer needed
    close(fig);
end