function output = scaleIR(input)
    % Scales impulse response so that its amplitude is between -1 and 1 in the
    % frequency domain.
    H = fft(input);
    Hs = H ./ max(abs(H));
    output = real(ifft(Hs));
end

