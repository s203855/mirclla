# MIRCLLA

## Measuring Impulse Responses for the Calibration of Large Loudspeaker Arrays
This is the repository for the project "Measuring Impulse Responses for the Calibration of Large Loudspeaker Arrays"


## TODO
- Split a "recorded" MESM signal into seperate channels
- Implement UI 
- Compare recordings using proper mic to ones using room mic
- Scale recordings
- check for system OS (if possible in plot function)
- determine points to get values from for validation
    - check both IR and measurement 

## Goals
### Base
- [x] Measure impulse responses
    - [x] Play impulse response
    - [x] Record impulse response
- [x] Calculate corresponding calibration filter
    - [x] Use impzest
    - [x] Read impulse response data
    - [x] truncate
    - [x] normalize
    - [x] Calculate inverse
### Extensions
- [ ] Speed up measurements
 - [ ] Multiple Exponential Sweep Method
- [x] Quick validation
    - [x] Measure impulse response
    - [x] Check if within expected values
- [x] User interface

