%% Home testing

function playFileHOME(file,scale)
fr = dsp.AudioFileReader("Filename",file);
dw = audioDeviceWriter("SampleRate",fr.SampleRate, "Device", "pipewire");

disp("Playing file");
while ~isDone(fr)
    aData = fr();
    aData = scale * aData;
    dw(aData);
end
disp("Done");

release(fr);
release(dw);
end
